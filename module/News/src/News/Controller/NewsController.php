<?php

namespace News\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use NewsComments\Model\NewsComments;
use NewsComments\Form\NewsCommentsForm;




class NewsController extends AbstractActionController 
{    
    /**
     * @var int
     */
    private $_perPage = 2;
    
    
    
    
    /**
     * Взаимодействие с моделью
     * 
     * @access public
     * @param string
     * @return object
     */
    public function getModelTable($model_table = 'News\Model\NewsTable')
    {
        $sm = $this->getServiceLocator();
        
        return $sm->get($model_table);
    }
    
    /**
	 * Список новостей
	 *
	 * @access	public
	 * @return	array
	 */
    public function indexAction()
    {
        $page = $this->params()->fromRoute('id', 1);
        
        if( ! $page)
        {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $news = $this->getModelTable()->fetchAll();
        $news->setCurrentPageNumber($page);        
        $news->setItemCountPerPage($this->_perPage);
    
        return array(
            'news'      => $news,
            'route' => array('route' => 'news'),
        );
    }
    
    /**
	 * Просмотр одной новости
	 *
	 * @access	public
	 * @return	array
	 */
    public function viewAction()
    {
        $message = '';
        $id = $this->params()->fromRoute('id', 0);

        if( ! $id || ! $one_news = $this->getModelTable()->getOneNews($id))
        {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $form = new NewsCommentsForm();
        $form->setData(array('news_id' => $one_news['id']));
        
        $comments_table = $this->getModelTable('NewsComments\Model\NewsCommentsTable');
        
        $request = $this->getRequest();
        if($request->isPost()) 
        {  
            $comments = new NewsComments();
            $form->setInputFilter($comments->getInputFilter());
            $form->setData($request->getPost());
            
            if($form->isValid()) 
            {
                $comments->exchangeArray($form->getData());                
                $comments_table->saveComment($comments);
                
                $form->setData(array('uname' => '', 'email' => '', 'text' => ''));
                $message = 'Ваш комментарий будет опубликован после одобрения модератором.';
            }
        }
                
        return array(
            'one_news'  => $one_news,
            'form'      => $form,
            'message'   => $message,
            'comments'  => $comments_table->getOneByNewsId($one_news['id']),
        );
    }
    
    /**
     * Поиск новостей
     * 
     * @access public
     * @return array
     */
    public function searchAction()
    {
        $text = $this->params()->fromRoute('text', '');
        $page = $this->params()->fromRoute('id', 1);
        
        $request = $this->getRequest();
        
        if(($is_post = $request->isPost() && $request->isPost('text')) || $text != '') 
        {                    
            if($is_post && $_POST['text'] != '')
                $text = trim(strip_tags($_POST['text']));  
            
            $str_len = mb_strlen($text);
            if($str_len > 3 && $str_len <= 255)
            {        
                $news = $this->getModelTable()->fetchAll($text);
                $news->setCurrentPageNumber($page);        
                $news->setItemCountPerPage($this->_perPage);
                                                    
                $view = new ViewModel(array(
                    'news' => $news,
                    'route' => array('route' => 'news_search', 'text' => $text),
                ));
                $view->setTemplate('news/news/index');
                return $view;
            }
            else
                $this->getResponse()->setStatusCode(404);
        }
        else
            $this->getResponse()->setStatusCode(404);
    }
}