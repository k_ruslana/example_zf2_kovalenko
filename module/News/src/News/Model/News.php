<?php

namespace News\Model;


class News 
{    
    public $id;
    public $title;
    public $img;
    public $text;
    
    
    /**
     * Заполнение свойств класса
     * 
     * @access public
     * @param array
     * @return void
     */
    public function exchangeArray($data)
    {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->title = (isset($data['title'])) ? $data['title'] : null;
        $this->img = (isset($data['img'])) ? $data['img'] : null;
        $this->text = (isset($data['text'])) ? $data['text'] : null;
    }
    
}