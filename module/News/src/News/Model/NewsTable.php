<?php

namespace News\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\AbstractSql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;




class NewsTable extends AbstractSql 
{
    /**
     * @var string
     */
    private $_table = 'pre_news';
    
    /**
     * @var string
     */
    private $_table_name = 'news';
    
    /**
     * @var Sql
     */
    private $_sql;
    
    /**
     * @var Select
     */
    private $_select;
    
    
    
    
    /**
     * Конструктор
     * 
     * @access public
     * @param Adapter
     * @return void
     */
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new News());

        $this->_sql = new Sql($this->adapter, $this->_table);
        $this->_select = $this->_sql->select();
    }
    
    /**
     * @access private
     * @return array
     */
    private function _executeStatement()
    {
        return $this->_sql->prepareStatementForSqlObject($this->_select)->execute();
    }
    
    /**
     * Список новостей с разбивкой на страницы
     * 
     * @access public
     * @param string
     * @return object
     */
    public function fetchAll($search_text = '')
    {                                
        $this->_select->columns(array('id', 'title', 'img', 'short_text', 'date_add'));
                      
        if($search_text != '')
            $this->_select->where('MATCH(title, text) AGAINST("'.$search_text.'*" IN BOOLEAN MODE)');                      
                      
        $paginatorAdapter = new DbSelect($this->_select, $this->adapter);
        $paginator = new Paginator($paginatorAdapter);
        
        return $paginator;  
    }
    
    /**
	 * Одна новость
	 * 
     * @access public
     * @param int
     * @return array or bool
	 */
    public function getOneNews($id)
    {
        $this->_select->where(array('id' => $id));   
        
        $row = $this->_executeStatement();
        
        return ($row->count()) ? $row->next() : false;
    }
    
    
}