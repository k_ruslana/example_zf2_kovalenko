<?php

namespace NewsComments\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;


class NewsComments implements InputFilterAwareInterface
{
    public $id;
    public $news_id;
    public $uname;
    public $email;
    public $text;
    protected $inputFilter;
    

    /**
     * Заполнение свойств класса
     * 
     * @access public
     * @param array
     * @return void
     */
    public function exchangeArray($data)
    {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->news_id = (isset($data['news_id'])) ? $data['news_id'] : null;
        $this->uname = (isset($data['uname'])) ? $data['uname'] : null;
        $this->email = (isset($data['email'])) ? $data['email'] : null;
        $this->text = (isset($data['text'])) ? $data['text'] : null;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    /**
     * Валидация и установка фильтров для формы
     * 
     * @access public
     * @return object
     */
    public function getInputFilter()
    {
        if( ! $this->inputFilter) 
        {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'uname',
                'required' => true,
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 3,
                            'max'      => 50,
                        ),
                    ),
                    array(
                        'name'    => 'Alnum',
                    ),
                ),
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'email',
                'required' => true,
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max'      => 100,
                        ),
                    ),
                    array(
                        'name'    => 'EmailAddress',
                    ),
                ),
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'text',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 3,
                        ),
                    ),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
    
}