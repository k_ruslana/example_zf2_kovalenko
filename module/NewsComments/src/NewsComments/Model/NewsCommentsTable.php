<?php

namespace NewsComments\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\AbstractSql;
use Zend\Db\Sql\Expression;




class NewsCommentsTable extends AbstractSql 
{
    /**
     * @var string
     */
    private $_table = 'pre_news_comments';
    
    /**
     * @var string
     */
    private $_table_name = 'news_comments';
    
    /**
     * @var Sql
     */
    private $_sql;
    
    /**
     * @var Select
     */
    private $_select;
    
    
    
    
    /**
     * Конструктор
     * 
     * @access public
     * @param Adapter
     * @return void
     */
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new NewsComments());

        $this->_sql = new Sql($this->adapter, $this->_table);
        $this->_select = $this->_sql->select();
    }
    
    /**
     * @access private
     * @return array
     */
    private function _executeStatement()
    {
        return $this->_sql->prepareStatementForSqlObject($this->_select)->execute();
    }
    
    /**
     * @access private
     * @return object
     */
    private function _executePrepareData($prepare_data)
    {
        return $this->adapter->query($this->_sql->getSqlStringForSqlObject($prepare_data), Adapter::QUERY_MODE_EXECUTE);
    }
    
    /**
	 * Один комментарий
	 * 
     * @access public
     * @param int
     * @return array or bool
	 */
    public function getOneComment($id)
    {
        $id = (int) $id;
        
        $this->_select->where(array('id' => $id));   
        
        $row = $this->_executeStatement();
        
        return ($row->count()) ? $row->next() : false;
    }
    
    /**
	 * Комментарии к одной новости
	 * 
     * @access public
     * @param int
     * @param string
     * @param string possible settings ASC or DESC
     * @return array
	 */
    function getOneByNewsId($news_id, $sort_field = 'date_add', $sort_by = 'DESC')
    {
        $this->_select->columns(array('uname', 'email', 'text', 'date_add'))
                      ->where('news_id = "'.$news_id.'" AND `check` = 1')
                      ->order($sort_field.' '.$sort_by);    
                      
        $result = $this->_executeStatement();   
        
        if($result->isQueryResult())   
        {
            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            
            return $resultSet->toArray();
        }
        else
            return array();
    }
    
    /**
	 * Сохранение комментаря к новости
	 * 
     * @access public
     * @param NewsComments
     * @return void
	 */
    public function saveComment(NewsComments $comment)
    {
        $data = array(
            'news_id'   => $comment->news_id,
            'uname'     => $comment->uname,
            'email'     => $comment->email,
            'text'      => $comment->text,
        );
        
        $id = (int) $comment->id;
        
        if($id == 0) 
        {
            $data['date_add'] = date('Y-m-d H:i:s');
            $this->_executePrepareData($this->_sql->insert()->values($data));
        }
    }
    
    
}