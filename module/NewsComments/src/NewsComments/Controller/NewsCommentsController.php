<?php

namespace NewsComments\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use NewsComments\Model\NewsComments; 
use NewsComments\Form\NewsCommentsForm;




class NewsCommentsController extends AbstractActionController 
{ 
    /**
     * Взаимодействие с моделью
     * 
     * @access public
     * @param string
     * @return object
     */
    public function getModelTable($model_table = 'NewsComments\Model\NewsCommentsTable')
    {
        $sm = $this->getServiceLocator();
        
        return $sm->get($model_table);
    }
    
    
    
    //--------------------------------------------- AJAX ------------------------------------------------------
    /**
     * Сортировка комментариев одной новости
     * 
     * @access public
     * @return string
     */
    function ajaxNewsIdCommentsAction()
    {
        $view = new ViewModel();
        $view->setTerminal(TRUE);
        
        $request = $this->getRequest();
        
        if($request->isXmlHttpRequest() && $request->isPost('data'))
        {
            $sort_field = array(
                                    'uname'     => 'имя пользователя', 
                                    'email'     => 'e-mail', 
                                    'date_add'  => 'дата добавления',
            ); 
            
            $sort_by = array(
                                'ASC'   => 'по возростанию', 
                                'DESC'  => 'по убыванию',
            );
            
            $result = array();
            
            $data = json_decode($request->getPost('data'));
            $data->news_id = (int) $data->news_id;
            
            if(isset($sort_field[$data->field]) && isset($sort_by[$data->by]) && $data->news_id > 0)
            {
                if($this->getModelTable('News\Model\NewsTable')->getOneNews($data->news_id))
                    $result = $this->getModelTable()->getOneByNewsId($data->news_id, $data->field, $data->by);
            }
            
            echo json_encode($result);
            return $view;
        }
        else
            $this->getResponse()->setStatusCode(404);                 
    }
    
    /**
	 * Сохранение одного комментария
	 *
	 * @access	public
	 * @return	string
	 */
    public function ajaxSaveCommentAction()
    {
        $view = new ViewModel();
        $view->setTerminal(TRUE);
        
        $request = $this->getRequest();
        
        if($request->isXmlHttpRequest() && $request->isPost('data')) 
        {
            $result = array('message' => '');
                        
            $data = json_decode($request->getPost('data'));            
            
            $data->news_id = (int) $data->news_id;
    
            if( ! $data->news_id || ! $one_news = $this->getModelTable('News\Model\NewsTable')->getOneNews($data->news_id))
            {
                $result['message'] = 'Ошибка! Новость не найдена.';
            }             
            else
            {
                $comments = new NewsComments();
                $form = new NewsCommentsForm();
                
                $form->setInputFilter($comments->getInputFilter());
                $form->setData((array) $data);
                
                if($form->isValid()) 
                {
                    $comments->exchangeArray($form->getData());                
                    $comments_table = $this->getModelTable()->saveComment($comments);
                    
                    $result['message'] = 'Ваш комментарий будет опубликован после одобрения модератором.';
                }
                else
                {
                    $errors = $form->getMessages();
                    
                    foreach($errors as $key => $arr)
                    {
                        foreach($arr as $val)
                            $result[$key][] = $val;
                    }
                }
            }
            
            echo json_encode($result);
            return $view;
        }
        else
            $this->getResponse()->setStatusCode(404);
    }
    //--------------------------------------------- END AJAX --------------------------------------------------
    

}