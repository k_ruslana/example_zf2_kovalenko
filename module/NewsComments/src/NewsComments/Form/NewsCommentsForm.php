<?php

namespace NewsComments\Form;

use Zend\Form\Form;


class NewsCommentsForm extends Form 
{
    /**
     * Конструктор формы
     * 
     * @return void
     */
    public function __construct()
    { 
        parent::__construct();
        
        $this->setAttribute('method', 'post');        
    
        $this->add(array(
            'name' => 'news_id',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'uname',
            'type' => 'Text',
            'required' => true,
            'options' => array(
                'label' => 'Имя пользователя',
            ),
            'attributes' => array(
                'required' => true,
            ),
        ));
        
        $this->add(array(
            'name' => 'email',
            'type' => 'Text',
            'required' => true,
            'options' => array(
                'label' => 'E-mail',
            ),
            'attributes' => array(
                'required' => true,
            ),
        ));
        
        $this->add(array(
            'name' => 'text',
            'type' => 'Textarea',
            'required' => true,
            'options' => array(
                'label' => 'Текст',
            ),
            'attributes' => array(
                'id'        => 'text',
                'required'  => true,
            ),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Добавить комментарий',
                'class' => 'btn2',
            ),
        ));
    }
}