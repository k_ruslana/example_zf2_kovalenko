<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'NewsComments\Controller\NewsComments' => 'NewsComments\Controller\NewsCommentsController',
        ),
    ),
    
    'router' => array(
        'routes' => array(
            'news_comments' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/news_comments[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-z-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'NewsComments\Controller\NewsComments',
                    ),
                ),
            ),
        ),
    ),
    
    'view_manager' => array(
        'template_path_stack' => array(
            'news_comments' => __DIR__ . '/../view',
        ),
    ),
);