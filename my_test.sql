-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Авг 27 2013 г., 15:36
-- Версия сервера: 5.5.23
-- Версия PHP: 5.4.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `my_test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `pre_news`
--

CREATE TABLE IF NOT EXISTS `pre_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT 'заголовок',
  `img` varchar(255) NOT NULL COMMENT 'картинка',
  `short_text` text NOT NULL COMMENT 'короткий текст',
  `text` text NOT NULL COMMENT 'полный текст',
  `date_add` datetime NOT NULL COMMENT 'дата добавления',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `title` (`title`,`text`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='новости' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pre_news_comments`
--

CREATE TABLE IF NOT EXISTS `pre_news_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(10) unsigned NOT NULL,
  `uname` varchar(50) NOT NULL COMMENT 'имя пользователя',
  `email` varchar(100) NOT NULL,
  `text` text NOT NULL COMMENT 'комментарий',
  `date_add` datetime NOT NULL COMMENT 'дата добавления',
  `check` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'статус модерации 0 - не проверено, 1 - проверено',
  PRIMARY KEY (`id`),
  KEY `news_id` (`news_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='комментарии новостей' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
