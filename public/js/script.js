$(document).ready(function() { 
   /* fancybox */
   if($('.preview').html() != undefined)
   { 
       $('.preview').fancybox({
        	openEffect	: 'elastic',
        	closeEffect	: 'elastic',
    
        	helpers : {
        		title : {
        			type : 'inside'
        		}
        	}
       });
   }
   /* end fancybox */ 
    
   /* validation */
   validation = {
                    'empty'         : 'Поле :field не должно быть пустым', 
                    'email'         : 'Поле :field имеет недопустимый адрес электронной почты. Введите его в формате имя@домен',
                    'min_len'       : 'Поле :field не может быть меньше :param символов',
                    'max_len'       : 'Поле :field не может быть больше :param символов',
                    'alpha_numeric' : 'Поле :field содержит недопустимые символы. Разрешены только буквенные символы и цифры',
   };
   error_message = '';
   
   $('#form_comment').submit(function(){
        var news_id = this.news_id;
        var uname = this.uname;
        var email = this.email;
        var text = this.text;
        
        var data = {
            'news_id'   : news_id.value,
            'uname'     : uname.value,
            'email'     : email.value,
            'text'      : text.value,
        };
        
        if($(this).validate_comment(data))
        {
            $.post('/news_comments/ajax-save-comment', {data:'{"news_id":"' + data.news_id + '", "uname":"' + data.uname + '", "email":"' + data.email + '", "text":"' + data.text + '"}'}, 
                function(data)
                {
                    if(data.message != '')
                    {
                        alert(data.message);
                        
                        uname.value = '';
                        email.value = '';
                        text.value = '';
                    }
                    else
                    {                        
                        $('.error').remove();
                        
                        $(uname).parent().after(print_error(data.uname));
                        $(email).parent().after(print_error(data.email));
                        $(text).after(print_error(data.text));
                    }
                }, 'json');
                
            return false;
        }
        else
            return false;
   });
   
   $('#search').submit(function(){
        var text = this.text.value;
        error_message = '';
        
        if(check_text(text, 'Поиск', true, 4))
        {
            alert(error_message);
            return false;
        }
   });
   
   $.fn.validate_comment = function(data)
   {
        var min_len = 3;
        error_message = '';
        
        if(check_text(data.uname, 'Имя пользователя', true, min_len, 50, 'alpha_numeric'))
        {
            alert(error_message);
            return false;
        }
        
        if(check_text(data.email, 'E-mail', true, min_len, 100, 'email'))
        {
            alert(error_message);
            return false;
        }
        
        if(check_text(data.text, 'Текст', true, min_len))
        {
            alert(error_message);
            return false;
        }
        
        return true;
   }
         
   function check_text(text, field, empty, min_len, max_len, check_key)
   {        
        switch(check_key)
        {
            case 'alpha_numeric':
                if( ! text.match(/^([a-zа-яё0-9]+)+$/gi))                   
                    error_message = str_replace(':field', field, validation['alpha_numeric']);
            break;
            case 'email':
                if( ! text.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/))                   
                    error_message = str_replace(':field', field, validation['email']);
            break;
        }
        
        text = trim(text);
        
        if(min_len != undefined && text.length < min_len)
            error_message = str_replace(':param', min_len, str_replace(':field', field, validation['min_len']));  
            
        if(max_len != undefined && text.length > max_len)
            error_message = str_replace(':param', max_len, str_replace(':field', field, validation['max_len']));   
                 
        if(text == '' && empty == null)
            error_message = str_replace(':field', field, validation['empty']); 
        
        return (error_message != '') ? true : false;
   }
   
   function print_error(errors)
   {
        var error = '';
        
        for(var key in errors)
            error += '<li>' + errors[key] + '</li>';
            
        return (error != '') ? '<ul class="error">' + error + '</ul>' : '';
   }
   
   function trim(str)
   {
        return str.replace(/^\s+|\s+$/g, '');
   }
   
   function str_replace(search, replace, str)
   {
        return str.replace(RegExp(search, 'g'), replace);
   }
   /* end validation */
   
   /* comments */
   if($('#sort_comment').html() != undefined)
   {
        var sort_comment = $('#sort_comment');
        var news_id = parseInt($('input[name=news_id]').val());
    
        var sort_field = {
                            'uname'     : 'имя пользователя', 
                            'email'     : 'e-mail', 
                            'date_add'  : 'дата добавления',
        }; 
        var field_id  = 'sort_field';
        
        var sort_by = {
                        'ASC'   : 'по возростанию', 
                        'DESC'  : 'по убыванию',
        };
        var by_id = 'sort_by';
        
        var select_field = create_select(field_id, sort_field, 'date_add');        
        $(select_field).bind({  
            change: function(e) {
                $(this).sort_comments(news_id);
            }
        });
        
        var select_by = create_select(by_id, sort_by, 'DESC');        
        $(select_by).bind({  
            change: function(e) {
                $(this).sort_comments(news_id);
            }
        });
        
        sort_comment.html('Сортировать по полю ').append(select_field).append(' по ').append(select_by);
   }
   
   function create_select(select_id, fields, selected_key)
   {
        var select_sort = document.createElement('select');
        select_sort.id = select_id;
        
        var option;
        
        for(var key in fields)
        {
            option = document.createElement('option');
            option.value = key;
            option.text = fields[key];
            
            if(key == selected_key)
                option.selected = 'selected';
                
            select_sort.appendChild(option);
        }
        
        return select_sort;
   }
        
   $.fn.sort_comments = function(news_id)
   {
        var selected_field = $('#' + field_id + ' option:selected').val();
        var selected_by = $('#' + by_id + ' option:selected').val();
        
        if(sort_field[selected_field] != undefined && sort_by[selected_by] && news_id > 0)
        {
            $.post('/news_comments/ajax-news-id-comments', {data:'{"field":"' + selected_field + '", "by":"' + selected_by + '", "news_id":"' + news_id + '"}'}, 
                function(data)
                {
                    var comments = $('.comments');
                    comments.html('');
                    
                    for(var key in data)
                    {
                        comments.append('<div>' + 
                            '<div class="gray">' + data[key].uname + ' ' + data[key].email + ' ' + format_date(data[key].date_add) + '</div>' +  
                            '<div>' + data[key].text + '</div>' + 
                            '</div><div class="clear"></div>');
                    }
                }, 'json');
        }      
            
        return this;
   } 
   
   function format_date(date)
   {
        var tmp_date = new Date(date);
        var day = tmp_date.getDate();
        var month = tmp_date.getMonth() + 1;
        var hours = tmp_date.getHours();
        var minutes = tmp_date.getMinutes();
        
        return format_number(day) + '.' + format_number(month) + '.' + tmp_date.getFullYear() + ' ' + format_number(hours) + ':' + format_number(minutes);
   }
   
   function format_number(number)
   {
        return (number < 10) ? '0' + number : number;
   }
   /* end comments */
   
});